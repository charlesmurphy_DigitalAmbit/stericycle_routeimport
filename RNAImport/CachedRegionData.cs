﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RNAImport.Apex;

namespace RNAImport
{
    public class CachedRegionData
    {
        public SingleRegionContext regionContext { get; set; }
        public UrlSet urlSet { get; set; }
    }
}
