﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ServiceModel;
using RNAImport.Apex;
using RNAImport.DBAccess;

namespace RNAImport
{
    class Program
    {
        static public string DATE_FORMAT = "yyyy-MM-dd";
        static public string TIMESPAN_FORMAT = "hh\\:mm\\:ss\\.fffffff";
        static public string DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss.fffffff";
        static IntegrationDBAccessor _IntegrationDBAccessor;
        static Dictionary<long, Dictionary<string, long>> BusinessUnitResourceExceptionRules = new Dictionary<long, Dictionary<string, long>>();
        static Dictionary<string, long> _ResourceExceptionRules = new Dictionary<string, long>();
        static string ROUTE_EXCEPTION_RULE_UNPLANNEDSTOP_IDENTIFIER = "UNPLANNEDSTOP";
        static string ROUTE_EXCEPTION_RULE_OUTOFBOUNDS_IDENTIFIER = "OUTOFBOUNDS";


        public enum ErrorLevel
        {
            None,           // success
            Transient,      // retry indefinitely - assumption is it will succeed eventually
            Partial,        // batch mix of success & failure - only relates to batch saves
            Fatal,          // never retry - assumption is it will never succeed
            Unknown         // retry n times as configured
        }

        static log4net.ILog Logger;
        static SessionCache SessionData;
        static string cacheFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "StericycleRnaRouteImportCache.json");

        static QueryServiceClient QueryServiceClient;
        static RoutingServiceClient DefaultSaveServiceClient;

        static void Main(string[] args)
        {
            
            log4net.Config.XmlConfigurator.Configure();
            Logger=CreateRollingFileLogger("Stericycle_RNA_RouteImport");
            SessionData=new SessionCache();
            _IntegrationDBAccessor=new IntegrationDBAccessor(Logger);
            string BU = string.Empty;
            string regionID = string.Empty;
            DateTime? afterDate = null;
            BusinessUnitResourceExceptionRules.Clear();
            _ResourceExceptionRules.Clear();
            bool completedSuccessfully = false;
            if (args.Length >= 3 )
            {
                DateTime temp; 
                if(!DateTime.TryParse(args[0],out temp))
                {
                    Console.WriteLine(string.Format("Date {0} is in an incorrect format. Please enter date in MM/DD/YYYY", args[0]));
                    Environment.Exit(10);
                }
               
                BU=args[1];
                regionID=args[2];
            }
            if (!afterDate.HasValue)
            {
                afterDate=Config.AfterDate;
                BU=Config.BusinessUnit;
            }

           
            while (!completedSuccessfully)
            {
                //LoadCachedUrls();
                Console.WriteLine("Start RNA Route Import");
                if (SessionData.SessionHeader==null)
                {
                    Login();
                    if (SessionData.SessionHeader!=null)
                    {
                        Region[] regions = RetrieveRegions();
                        List<BusinessUnit> BusinessUnits = RetrieveBusinessUnit();
                        _ResourceExceptionRules.Clear();
                        foreach ( BusinessUnit businessUnit in BusinessUnits)
                        {
                            List<ResourceExceptionRule> reRules = new List<ResourceExceptionRule>();
                            ErrorLevel errorLevel;

                            if (RetrieveResourceExceptionRules(businessUnit.EntityKey,out reRules, out errorLevel))
                            {
                                switch (errorLevel)
                                {
                                    case ErrorLevel.None:
                                        BusinessUnitResourceExceptionRules.Add(businessUnit.EntityKey, reRules.Cast<ResourceExceptionRule>()
                                                            .Where(rer => !rer.IsDeleted&&rer.IsEnabled&&(rer.Identifier==ROUTE_EXCEPTION_RULE_OUTOFBOUNDS_IDENTIFIER||rer.Identifier==ROUTE_EXCEPTION_RULE_UNPLANNEDSTOP_IDENTIFIER))
                                                            .GroupBy(rer => rer.Identifier).ToDictionary(group => group.Key, group => group.First().EntityKey));
                                        break;
                                    case ErrorLevel.Transient:
                                        // retry on next interval
                                        break;
                                    case ErrorLevel.Partial:
                                        // error cannot occur
                                        break;
                                    case ErrorLevel.Fatal:

                                        break;
                                    case ErrorLevel.Unknown:
                                        throw new Exception();

                                }
                               

                            }
                            
                        }
                        long? BUEntityKey = BusinessUnits.SingleOrDefault(x => x.Identifier==BU)?.EntityKey;


                        

                        foreach (Region region in regions)
                        {
                            if (!BUEntityKey.HasValue)
                            {
                                BUEntityKey=region.BusinessUnitEntityKey;
                            }
                            
                            // if region is in the configured list of regions to process here
                            // then get URLS for it.
                            if ((Config.RegionFilter.Contains(region.Identifier.ToUpper()) || region.Identifier.ToUpper() == regionID.ToUpper() 
                                || regionID.ToUpper() == "ALL") && region.BusinessUnitEntityKey == BUEntityKey)
                            {
                                SingleRegionContext context = new SingleRegionContext
                                {
                                    RegionEntityKey=region.EntityKey,
                                    BusinessUnitEntityKey=region.BusinessUnitEntityKey
                                };

                                UrlSet regionUrls = RetrieveUrlSetByRegionContext(context);
                                if (regionUrls!=null)
                                {
                                    SessionData.CachedRegionStore.Add(new CachedRegionData
                                    {
                                        regionContext=context,
                                        urlSet=regionUrls
                                    });
                                }
                            }
                        }
                        //WriteCachedUrls();
                    }
                }

                if (SessionData.SessionHeader!=null&&SessionData.CachedRegionStore!=null)
                {
                    bool errorEncountered = false;
                    foreach (CachedRegionData regionData in SessionData.CachedRegionStore)
                    {
                        _ResourceExceptionRules=BusinessUnitResourceExceptionRules[(long)regionData.regionContext.BusinessUnitEntityKey];

                        List<Route>retrievedRoutes=RetrieveRoutes(regionData, afterDate);
                        if (InsertRoutes(regionData,retrievedRoutes))
                        {
                            Console.WriteLine(string.Format("Inserted {0} Routes into database successfully", retrievedRoutes.Count));
                            Logger.Info("Inserted Route Successfully");
                        } else
                        {
                            Console.WriteLine("Error Inserting Routes. See Logs at " + Config.LogFilePath);
                            Logger.Error("Error Inserting Routes");
                        }
                        
                    }
                    if (!errorEncountered)
                    {
                        completedSuccessfully=true;
                    }
                }

                if (!completedSuccessfully)
                {
                    Logger.WarnFormat("Reached end of processing without completing. Pausing for {0} seconds before repeating process.", Config.SleepDuration/1000);
                    System.Threading.Thread.Sleep(Config.SleepDuration);
                }
            }
        }

        static void LoadCachedUrls()
        {
            if (!File.Exists(cacheFile))
            {
                Logger.Info("No cache file exists");
            }
            else
            {
                Logger.InfoFormat("Loading cache file from {0}", cacheFile);
                try
                {
                    string jsonData = File.ReadAllText(cacheFile);
                    SessionCache cachedData = Newtonsoft.Json.JsonConvert.DeserializeObject<SessionCache>(jsonData);
                    if (cachedData!=null)
                    {
                        SessionData=cachedData;
                        DefaultSaveServiceClient=new RoutingServiceClient("BasicHttpBinding_IRoutingService", SessionData.DefaultSaveServiceClientUrl);
                        QueryServiceClient=new QueryServiceClient("BasicHttpBinding_IQueryService", SessionData.QueryServiceClientUrl);
                    }
                }
                catch (Exception ex)
                {
                    Logger.ErrorFormat("Error opening cache file: {0}", ex.Message);
                }
            }
        }

        static void WriteCachedUrls()
        {
            Logger.InfoFormat("Writing cache file to {0}", cacheFile);
            try
            {
                using (StreamWriter writer = new StreamWriter(cacheFile, append: false))
                {
                    Newtonsoft.Json.JsonSerializerSettings settings = new Newtonsoft.Json.JsonSerializerSettings
                    {
                        PreserveReferencesHandling=Newtonsoft.Json.PreserveReferencesHandling.None,
                        ReferenceLoopHandling=Newtonsoft.Json.ReferenceLoopHandling.Ignore
                    };

                    string jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(SessionData, Newtonsoft.Json.Formatting.None, settings);

                    writer.Write(jsonData);
                }
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error writing cache file: {0}", ex.Message);
            }
        }

        static void ResetSessionCache()
        {
            SessionData.Reset();
            Logger.InfoFormat("Removing cache file at {0}", cacheFile);
            try
            {
                File.Delete(cacheFile);
            }
            catch (Exception ex)
            {
                Logger.ErrorFormat("Error deleteing cache file: {0}", ex.Message);
            }
        }

        static void Login()
        {
            try
            {
                Logger.Debug("Begin Login process.");

                LoginServiceClient loginServiceClient = new LoginServiceClient();
                LoginResult loginResult = loginServiceClient.Login(
                    Config.RNALogin,
                    Config.RNAPassword,
                    new CultureOptions(),
                    new ClientApplicationInfo
                    {
                        ClientApplicationIdentifier=new Guid(Config.RNAAppIdentifier)
                    });

                if (loginResult==null)
                {
                    Logger.Error("Login failed.");
                }
                else
                {
                    Logger.Debug("Login completed successfully. User EntityKey: "+loginResult.User.EntityKey);

                    SessionData.UserEntityKey=loginResult.User.EntityKey;

                    Logger.Debug("Storing SessionHeader: "+loginResult.UserSession.Guid);
                    SessionData.SessionHeader=new SessionHeader { SessionGuid=loginResult.UserSession.Guid };

                    Logger.Debug("Create QueryServiceClient for: "+loginResult.QueryServiceUrl);
                    SessionData.QueryServiceClientUrl=loginResult.QueryServiceUrl;
                    QueryServiceClient=new QueryServiceClient("BasicHttpBinding_IQueryService", loginResult.QueryServiceUrl);

                    Logger.Debug("Create DefaultRoutingServiceClient for: "+loginResult.DefaultRoutingServiceUrl);
                    SessionData.DefaultSaveServiceClientUrl=loginResult.DefaultRoutingServiceUrl;
                    DefaultSaveServiceClient=new RoutingServiceClient("BasicHttpBinding_IRoutingService", loginResult.DefaultRoutingServiceUrl);


                    Logger.Debug("Login process complete.");
                }
            }
            catch (FaultException<TransferErrorCode> tec)
            {
                Logger.Error("TransferErrorCode caught: "+tec.Action+" | "+tec.Code.Name+" | "+tec.Detail.ErrorCode_Status+" | "+tec.Message);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
        }

        static Region[] RetrieveRegions()
        {
            try
            {
                Logger.Debug("Begin Retrieve Regions process.");

                RetrievalResults retrievalResults = QueryServiceClient.RetrieveRegionsGrantingPermissions(
                    SessionData.SessionHeader,
                    new RolePermission[] { },
                    false);

                if (retrievalResults.Items==null)
                {
                    Logger.Error("Retrieve Regions failed.");
                }
                else if (retrievalResults.Items.Length==0)
                {
                    Logger.Error("No Regions exist.");
                }
                else
                {
                    Logger.Debug("Retrieve Regions completed successfully.");
                    return retrievalResults.Items.Cast<Region>().ToArray();
                }
            }
            catch (FaultException<TransferErrorCode> tec)
            {
                if (tec.Detail.ErrorCode_Status=="SessionAuthenticationFailed"||tec.Detail.ErrorCode_Status=="InvalidEndpointRequest")
                {
                    Logger.Warn("Resetting Session Cache");
                    ResetSessionCache();
                }
                else
                {
                    Logger.Error("TransferErrorCode caught: "+tec.Action+" | "+tec.Code.Name+" | "+tec.Detail.ErrorCode_Status+" | "+tec.Message, tec);
                    throw;
                }
            }
            catch (EndpointNotFoundException nfex)
            {
                Logger.Warn("Endpoint not found. Resetting Session Cache");
                ResetSessionCache();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                throw;
            }
            return null;
        }
        static List<BusinessUnit> RetrieveBusinessUnit()
        {
            try
            {
                Logger.Debug("Begin Retrieve Business Units process.");

                RetrievalResults retrievalResults = QueryServiceClient.RetrieveBusinessUnitsGrantingPermissions(
                    SessionData.SessionHeader,
                    new RolePermission[] { },
                    false);

                if (retrievalResults.Items==null)
                {
                    Logger.Error("Retrieve Business Units failed.");
                }
                else if (retrievalResults.Items.Length==0)
                {
                    Logger.Error("No Business Units exist.");
                }
                else
                {
                    Logger.Debug("Retrieve Business Units completed successfully.");
                    return retrievalResults.Items.Cast<BusinessUnit>().ToList();
                }
            }
            catch (FaultException<TransferErrorCode> tec)
            {
                if (tec.Detail.ErrorCode_Status=="SessionAuthenticationFailed"||tec.Detail.ErrorCode_Status=="InvalidEndpointRequest")
                {
                    Logger.Warn("Resetting Session Cache");
                    ResetSessionCache();
                }
                else
                {
                    Logger.Error("TransferErrorCode caught: "+tec.Action+" | "+tec.Code.Name+" | "+tec.Detail.ErrorCode_Status+" | "+tec.Message, tec);
                    throw;
                }
            }
            catch (EndpointNotFoundException nfex)
            {
                Logger.Warn("Endpoint not found. Resetting Session Cache");
                ResetSessionCache();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                throw;
            }
            return null;
        }
        static UrlSet RetrieveUrlSetByRegionContext(RegionContext context)
        {
            UrlSet urlSet = null;
            try
            {
                Logger.Debug("Begin Retrieve Urls process.");
                urlSet=QueryServiceClient.RetrieveUrlsForContext(SessionData.SessionHeader, context);
            }
            catch (FaultException<TransferErrorCode> tec)
            {
                Logger.Error("TransferErrorCode caught: "+tec.Action+" | "+tec.Code.Name+" | "+tec.Detail.ErrorCode_Status+" | "+tec.Message);
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
            return urlSet;
        }

        static log4net.ILog CreateRollingFileLogger(string logName)
        {
            log4net.Layout.PatternLayout layout = new log4net.Layout.PatternLayout("%date | %-5level | %message%newline");
            layout.ActivateOptions();
            log4net.Appender.RollingFileAppender appender = new log4net.Appender.RollingFileAppender();
            appender.Name="RollingFileAppender_"+logName;
            appender.File=Config.LogFilePath+logName+"_.txt";
            appender.LockingModel=new log4net.Appender.FileAppender.MinimalLock();
            appender.RollingStyle=log4net.Appender.RollingFileAppender.RollingMode.Date;
            appender.DatePattern="yyyyMMdd";
            appender.AppendToFile=true;
            appender.StaticLogFileName=false;
            appender.PreserveLogFileNameExtension=true;
            appender.Layout=layout;
            appender.ActivateOptions();
            log4net.ILog log = log4net.LogManager.GetLogger(logName);
            log4net.Repository.Hierarchy.Logger logger = (log4net.Repository.Hierarchy.Logger)log.Logger;
            logger.AddAppender(appender);
            return log;
        }

        static List<Route> RetrieveRoutes(CachedRegionData regionData, DateTime? dateTime)
        {
            List<Route> retreivedRoutes = new List<Route>();
            int pageIndex = 0;
            long pageLength = 1;
           
            do
            {
                try
                {
                    Logger.Debug("Begin Retrieve Routes  process.");

                    RetrievalResults retrievalResults = QueryServiceClient.Retrieve(
                        SessionData.SessionHeader,
                        regionData.regionContext,
                        new RetrievalOptions
                        {
                            Expression=new AndExpression
                            {
                                Expressions=new SimpleExpressionBase[]
                                {
                                    new EqualToExpression
                                    {
                                        Left = new PropertyExpression
                                        {
                                            Name = "RouteState_State"
                                        },
                                        Right = new ValueExpression
                                        {
                                            Value = "Completed"
                                        }
                                    },
                                    new GreaterThanExpression
                                    {
                                         Left = new PropertyExpression
                                        {
                                            Name = "RoutingSessionDate"
                                        },
                                        Right = new ValueExpression
                                        {
                                            Value = dateTime.Value
                                        }
                                    }
                                }
                            },

                            PropertyInclusionMode=PropertyInclusionMode.AccordingToPropertyOptions,
                            PropertyOptions=new RoutePropertyOptions
                            {
                                ArrivalTime=true,
                                CompleteTime=true,
                                DepartureTime=true,
                                Description=true,
                                Equipment=true,
                                EquipmentOptions=new RouteEquipmentPropertyOptions
                                {
                                    EquipmentEntityKey=true,
                                    EquipmentIdentifier=true,
                                    EquipmentTypeEntityKey=true
                                },
                                Identifier=true,
                                NumberOfCancelledStops=true,
                                NumberOfCompletedServiceableStops=true,
                                NumberOfMissedServiceWindows=true,
                                NumberOfNonServiceableStops=true,
                                NumberOfServiceableStops=true,
                                NumberOfUnknownStops=true,
                                PlannedNumberOfServiceableStops=true,
                                RegionEntityKey=true,
                                RegionIdentifier= true,
                                RoutingSessionDate=true,
                                StartTime=true,
                                Stops=true,
                                StopsOptions=new StopPropertyOptions
                                {
                                    Actions=true,
                                    ActionsOptions=new StopActionPropertyOptions
                                    {
                                        OrderIdentifier=true,
                                        StopActionLineItemQuantities=true,
                                        StopActionLineItemQuantitiesOptions=new StopActionLineItemQuantitiesPropertyOptions
                                        {
                                            LineItem=true,
                                            LineItemOptions=new LineItemPropertyOptions
                                            {
                                                QuantityInputQuality_QuantityInputQuality=true
                                            }
                                        }
                                    },
                                    ArrivalTime=true,
                                    DepartureTime=true,
                                    PlannedSequenceNumber=true,
                                    SequenceNumber=true
                                },
                                TotalBreakTime=true,
                                TotalDistance=true,
                                TotalPlannedDistance=true,
                                TotalPlannedServiceTime=true,
                                TotalPlannedTravelTime=true,
                                TotalServiceTime=true,
                                TotalTravelTime=true,
                                Workers=true,
                                WorkersOptions=new RouteWorkerPropertyOptions
                                {
                                    WorkerIdentifier=true
                                }
                            },
                            Paged=true,
                            PageIndex=pageIndex,
                            PageSize=1500,
                            ReturnTotalPages=true,
                            Type="Route"
                        });

                    if (retrievalResults.Items==null)
                    {
                        Logger.Error("Retrieve Routes failed.");
                    }
                    else if (retrievalResults.Items.Length==0)
                    {
                        Logger.Error("No Routes exist.");
                    }
                    else
                    {
                        pageLength=retrievalResults.TotalPages;
                        pageIndex++;
                        Logger.Debug("Retrieve Routes completed successfully.");
                        retreivedRoutes.AddRange(retrievalResults.Items.Cast<Route>().ToList());
                    }
                }
                catch (FaultException<TransferErrorCode> tec)
                {
                    if (tec.Detail.ErrorCode_Status=="SessionAuthenticationFailed"||tec.Detail.ErrorCode_Status=="InvalidEndpointRequest")
                    {
                        Logger.Warn("Resetting Session Cache");
                        ResetSessionCache();
                    }
                    else
                    {
                        Logger.Error("TransferErrorCode caught: "+tec.Action+" | "+tec.Code.Name+" | "+tec.Detail.ErrorCode_Status+" | "+tec.Message, tec);
                        throw;
                    }
                }
                catch (EndpointNotFoundException nfex)
                {
                    Logger.Warn("Endpoint not found. Resetting Session Cache");
                    ResetSessionCache();
                }
                catch (Exception ex)
                {
                    Logger.Error(ex.Message, ex);
                    throw;
                }
            } while (pageIndex<pageLength);
            return retreivedRoutes;
        }

        static bool InsertRoutes(CachedRegionData regionData, List<Route> routes)
        {
            foreach (Route route in routes)
            {
                try
                {
                    RouteEquipmentBase routeEquipmentBase = route.Equipment?.FirstOrDefault();
                    string equipmentIdentifier = string.Empty;
                    string equipmentTypeIdentifier = string.Empty;
                    if (RetrieveResourceExceptions(out ResourceException[] resourceExceptions, route.EntityKey, regionData))
                    {
                        string[] lineItemQuantityInputQualities = route.Stops?.OfType<ServiceableStop>().SelectMany(stop => stop.Actions).SelectMany(action => action.StopActionLineItemQuantities).Select(saliq => saliq.LineItem.QuantityInputQuality_QuantityInputQuality).ToArray();
                        Stop[] routeDelayStops = route.Stops?.OfType<RouteDelayStop>().ToArray();
                        long[] unplannedStopEntityKeys = resourceExceptions.Where(re => re.RuleEntityKey==_ResourceExceptionRules[ROUTE_EXCEPTION_RULE_UNPLANNEDSTOP_IDENTIFIER]&&re.StopEntityKey.HasValue)
                                                                           .Select(re => re.StopEntityKey.Value).ToArray();
                        Stop[] unplannedStops = route.Stops?.Where(stop => unplannedStopEntityKeys.Contains(stop.EntityKey)).ToArray();


                        

                    if (routeEquipmentBase is RouteEquipment routeEquipment)
                        {
                            equipmentIdentifier=routeEquipment.EquipmentIdentifier;
                            if (RetrieveEquipment(out Equipment equipment, routeEquipment.EquipmentEntityKey, regionData))
                            {
                                long? equipmentTypeEntityKey = equipment?.EquipmentTypeEntityKey;
                                if (equipmentTypeEntityKey.HasValue&&
                                    RetrieveEquipmentType(out EquipmentType equipmentType, equipmentTypeEntityKey.Value, regionData))
                                {
                                    equipmentTypeIdentifier=equipmentType?.Identifier;
                                }
                            }
                        }



                        if (!_IntegrationDBAccessor.InsertStagedRoute(
                                         route.RegionIdentifier,
                                         route.RegionEntityKey,
                                         DateTime.Parse(route.RoutingSessionDate),
                                         route.Identifier,
                                         route.EntityKey,
                                         route.Description,
                                         route.StartTime?.Value,
                                         route.DepartureTime?.Value,
                                         route.ArrivalTime?.Value,
                                         route.CompleteTime?.Value,
                                         route.TotalPlannedDistance?.Value,
                                         route.TotalDistance?.Value,
                                         route.TotalPlannedServiceTime,
                                         route.TotalServiceTime,
                                         route.TotalPlannedTravelTime,
                                         route.TotalTravelTime,
                                         route.TotalBreakTime,
                                         (short?)route.PlannedNumberOfServiceableStops,
                                         (short?)route.NumberOfCancelledStops,
                                         (short?)route.NumberOfCompletedServiceableStops,
                                         (short?)route.NumberOfMissedServiceWindows,
                                         (short?)route.NumberOfNonServiceableStops,
                                         (short?)route.NumberOfServiceableStops,
                                         (short?)route.NumberOfUnknownStops,
                                         (short?)route.Stops?.OfType<ServiceableStop>().SelectMany(stop => stop.Actions).Select(stopAction => stopAction.OrderIdentifier).Distinct().Count(),
                                         (short?)lineItemQuantityInputQualities.Count(quality => quality!=nameof(QuantityInputQuality.NotSet)),
                                         (short?)lineItemQuantityInputQualities.Count(quality => quality==nameof(QuantityInputQuality.Scanned)||quality==nameof(QuantityInputQuality.GroupScanned)),
                                         (short?)route.Stops?.OfType<ServiceableStop>().Count(stop => stop.PlannedSequenceNumber!=stop.SequenceNumber),
                                         (short?)routeDelayStops?.Length,
                                         SumStopTimes(routeDelayStops),
                                         (short?)resourceExceptions.Count(re => re.RuleEntityKey==_ResourceExceptionRules[ROUTE_EXCEPTION_RULE_OUTOFBOUNDS_IDENTIFIER]),
                                         (short?)unplannedStops?.Length,
                                         SumStopTimes(unplannedStops),
                                         equipmentTypeIdentifier,
                                         equipmentIdentifier,
                                         route.Workers?.FirstOrDefault()?.WorkerIdentifier))
                    {
                            Logger.ErrorFormat("Error Inserting Route{0} Region{1} SessionDate {2} ", route.Identifier, route.RegionIdentifier, route.RoutingSessionDate);
                        }
                    }
                }catch(Exception ex)
                {
                    Logger.Error("Error Inserting Route{0} Region{1} SessionDate {2} | ", ex);
                    return false;
                }
               
            }

            return true;
        }

        static TimeSpan? SumStopTimes(Stop[] stops)
        {
            TimeSpan? totalStopTime = null;
            if (stops!=null)
            {
                totalStopTime=new TimeSpan();
                foreach (Stop stop in stops.Where(s => s.ArrivalTime!=null&&s.DepartureTime!=null))
                {
                    totalStopTime.Value.Add(stop.DepartureTime.Value-stop.ArrivalTime.Value);
                }
            }
            return totalStopTime;
        }

        static bool RetrieveEquipment(out Equipment equipment, long equipmentEntityKey, CachedRegionData regionData)
        {
            ErrorLevel errorLevel = ErrorLevel.None;
            bool success = false;
            equipment=null;
            Logger.Debug("Retrieve Equipment: "+equipmentEntityKey);
            List<Equipment> equips = RetrieveEquipRNA(regionData, equipmentEntityKey, out errorLevel);
            switch (errorLevel)
            {
                case ErrorLevel.None:
                    if (equips.Count==0)
                    {
                        Logger.Warn("Equipment no longer exists: "+equipmentEntityKey);
                    }
                    equipment=equips.FirstOrDefault();
                    success=true;
                    break;
                case ErrorLevel.Transient:
                    // retry on next interval
                    break;
                case ErrorLevel.Partial:
                    // error cannot occur
                    break;
                case ErrorLevel.Fatal:
                    
                    break;
                case ErrorLevel.Unknown:
                    throw new Exception();
            }
            return success;
        }

        static bool RetrieveEquipmentType(out EquipmentType equipmentType, long equipmentTypeEntityKey, CachedRegionData regionData)
        {
            ErrorLevel errorLevel = ErrorLevel.None;
            bool success = false;
            equipmentType=null;
            Logger.Debug("Retrieve Equipment Type: "+equipmentTypeEntityKey);
            List<EquipmentType> equipmentTypes = RetrieveEquipTypeRNA(regionData, equipmentTypeEntityKey, out errorLevel);
            switch (errorLevel)
            {
                case ErrorLevel.None:
                    if (equipmentTypes.Count==0)
                    {
                        Logger.Warn("Equipment Type no longer exists: "+equipmentTypeEntityKey);
                    }
                    equipmentType=equipmentTypes.FirstOrDefault();
                    success=true;
                    break;
                case ErrorLevel.Transient:
                    // retry on next interval
                    break;
                case ErrorLevel.Partial:
                    // error cannot occur
                    break;
                case ErrorLevel.Fatal:
                    
                    break;
                case ErrorLevel.Unknown:
                    throw new Exception();
            }
            return success;
        }

        static bool RetrieveResourceExceptions(out ResourceException[] resourceExceptions, long routeEntityKey, CachedRegionData regionData)
        {
            ErrorLevel errorLevel = ErrorLevel.None;
            bool success = false;
            Logger.Debug("Retrieve Resource Exceptions: "+routeEntityKey);
            resourceExceptions=RetrieveResourceExceptionsRNA(regionData, routeEntityKey, _ResourceExceptionRules.Select(pair => pair.Value).ToArray());
            switch (errorLevel)
            {
                case ErrorLevel.None:
                    success=true;
                    break;
                case ErrorLevel.Transient:
                    // retry on next interval
                    break;
                case ErrorLevel.Partial:
                    // error cannot occur
                    break;
                case ErrorLevel.Fatal:
                    
                    break;
                case ErrorLevel.Unknown:
                    throw new Exception();
            }
            return success;
        }

        static List<Equipment> RetrieveEquipRNA(CachedRegionData regionData, long equipmentEntityKey, out ErrorLevel errorLevel )
        {
            errorLevel=ErrorLevel.None;
            
            try
            {
                Logger.Debug("Begin Retrieve Equipment process.");

                RetrievalResults retrievalResults = QueryServiceClient.Retrieve(
                    SessionData.SessionHeader,
                    regionData.regionContext,
                    new RetrievalOptions
                    {
                        Expression= new EqualToExpression
                        {
                            Left = new PropertyExpression
                            {
                                Name ="EntityKey"
                            }, 
                            Right = new ValueExpression
                            {
                                Value =equipmentEntityKey
                            }
                          
                        },
                        PropertyInclusionMode=PropertyInclusionMode.AccordingToPropertyOptions,
                        PropertyOptions=new EquipmentPropertyOptions
                        {
                            Identifier = true,
                            EquipmentTypeEntityKey = true
                        },
                        Paged=false,
                        Type="Equipment"
                    });
                if (retrievalResults.Items==null)
                {
                    Logger.Error("Retrieve Equipment failed.");
                    errorLevel=ErrorLevel.Fatal;
                }
                else if (retrievalResults.Items.Length==0)
                {
                    Logger.ErrorFormat("No Equipment with Entity Key {0} exist.", equipmentEntityKey);
                    errorLevel=ErrorLevel.None;
                }
                else
                {
                    Logger.DebugFormat("Retrieved Equipment with Entity Key {0} successfully.", equipmentEntityKey);
                    return retrievalResults.Items.Cast<Equipment>().ToList();
                }
            }
            catch (FaultException<TransferErrorCode> tec)
            {
                if (tec.Detail.ErrorCode_Status=="SessionAuthenticationFailed"||tec.Detail.ErrorCode_Status=="InvalidEndpointRequest")
                {
                    Logger.Warn("Resetting Session Cache");
                    ResetSessionCache();
                    errorLevel=ErrorLevel.Transient;
                }
                else
                {
                    Logger.Error("TransferErrorCode caught: "+tec.Action+" | "+tec.Code.Name+" | "+tec.Detail.ErrorCode_Status+" | "+tec.Message, tec);
                    errorLevel=ErrorLevel.Fatal;
                    throw;
                }
            }
            catch (EndpointNotFoundException nfex)
            {
                Logger.Warn("Endpoint not found. Resetting Session Cache");
                ResetSessionCache();
                errorLevel=ErrorLevel.Transient;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                errorLevel=ErrorLevel.Fatal;
                throw;
            }
            return null;
        }

        static List<EquipmentType> RetrieveEquipTypeRNA(CachedRegionData regionData, long equipmentTypeEntityKey, out ErrorLevel errorLevel)
        {
            errorLevel=ErrorLevel.None;
            try
            {
                Logger.Debug("Begin Retrieve Equipment Type process.");

                RetrievalResults retrievalResults = QueryServiceClient.Retrieve(
                    SessionData.SessionHeader,
                    regionData.regionContext,
                    new RetrievalOptions
                    {
                        Expression=new EqualToExpression
                        {
                            Left=new PropertyExpression
                            {
                                Name="EntityKey"
                            },
                            Right=new ValueExpression
                            {
                                Value=equipmentTypeEntityKey
                            }

                        },
                        PropertyInclusionMode=PropertyInclusionMode.AccordingToPropertyOptions,
                        PropertyOptions=new EquipmentTypePropertyOptions
                        {
                            Identifier = true
                        },
                        Paged=false,
                        Type="EquipmentType"
                    });
                if (retrievalResults.Items==null)
                {
                    Logger.Error("Retrieve Equipment Type failed.");
                    errorLevel=ErrorLevel.Fatal;
                }
                else if (retrievalResults.Items.Length==0)
                {
                    errorLevel=ErrorLevel.None;
                    Logger.ErrorFormat("No Equipment Type with Entity Key {0} exist.", equipmentTypeEntityKey);
                }
                else
                {
                    errorLevel=ErrorLevel.None;
                    Logger.DebugFormat("Retrieved Equipment Type with Entity Key {0} successfully.", equipmentTypeEntityKey);
                    return retrievalResults.Items.Cast<EquipmentType>().ToList();
                }
            }
            catch (FaultException<TransferErrorCode> tec)
            {
                if (tec.Detail.ErrorCode_Status=="SessionAuthenticationFailed"||tec.Detail.ErrorCode_Status=="InvalidEndpointRequest")
                {
                    errorLevel=ErrorLevel.Transient;
                    Logger.Warn("Resetting Session Cache");
                    ResetSessionCache();
                }
                else
                {
                    errorLevel=ErrorLevel.Fatal;
                    Logger.Error("TransferErrorCode caught: "+tec.Action+" | "+tec.Code.Name+" | "+tec.Detail.ErrorCode_Status+" | "+tec.Message, tec);
                    throw;
                }
            }
            catch (EndpointNotFoundException nfex)
            {
                errorLevel=ErrorLevel.Transient;
                Logger.Warn("Endpoint not found. Resetting Session Cache");
                ResetSessionCache();
            }
            catch (Exception ex)
            {
                errorLevel=ErrorLevel.Fatal;
                Logger.Error(ex.Message, ex);
                throw;
            }
            return null;
        }

        static ResourceException[] RetrieveResourceExceptionsRNA(CachedRegionData regionData, long routeEntityKey, long[] ruleEntityKeys)
        {
           
            RetrievalResults retrievalResults = QueryServiceClient.Retrieve(
                SessionData.SessionHeader,
                    regionData.regionContext,
                new RetrievalOptions
                {
                    Expression=new AndExpression
                    {
                        Expressions=new SimpleExpressionBase[]
                        {
                            new EqualToExpression
                            {
                                Left = new PropertyExpression { Name = "RouteEntityKey" },
                                Right = new ValueExpression { Value = routeEntityKey }
                            },
                            new InExpression
                            {
                                Left = new PropertyExpression { Name = "RuleEntityKey" },
                                Right = new ValueExpression { Value = ruleEntityKeys }
                            }
                        }
                    },
                    PropertyInclusionMode=PropertyInclusionMode.AccordingToPropertyOptions,
                    PropertyOptions=new ResourceExceptionPropertyOptions
                    {
                        RuleEntityKey=true,
                        StopEntityKey=true
                    },
                    Type=nameof(ResourceException)
                });
            return (retrievalResults!=null&&retrievalResults.Items!=null) ? retrievalResults.Items.Cast<ResourceException>().ToArray() : null;
        }

        static bool RetrieveResourceExceptionRules(long businessUnitEntityKey, out List<ResourceExceptionRule> reRules, out ErrorLevel errorLevel)
        {
            errorLevel=ErrorLevel.None;
            reRules = new List<ResourceExceptionRule>();
            int pageIndex = 0;
            long totalPages = 1;

           
                try
                {
                    do
                    {
                        Logger.Debug("Begin Retrieve ResourceExceptionRule process.");

                        RetrievalResults retrievalResults = QueryServiceClient.Retrieve(
                            SessionData.SessionHeader,
                            new MultipleRegionContext
                            {
                                BusinessUnitEntityKey=businessUnitEntityKey,
                                Mode=MultipleRegionMode.All
                            },
                            new RetrievalOptions
                            {
                                PropertyInclusionMode=PropertyInclusionMode.AccordingToPropertyOptions,
                                PropertyOptions=new ResourceExceptionRulePropertyOptions
                                {
                                    Identifier=true,
                                    IsDeleted=true,
                                    IsEnabled=true
                                },
                                Paged=true,
                                PageIndex = pageIndex,
                                PageSize = 1000,
                                ReturnTotalPages = true,
                                Type="ResourceExceptionRule"
                            });
                        if (retrievalResults.Items==null)
                        {
                            Logger.Error("Retrieve ResourceExceptionRule failed.");
                            errorLevel=ErrorLevel.Fatal;
                        }
                        else if (retrievalResults.Items.Length==0)
                        {
                            errorLevel=ErrorLevel.None;
                            Logger.ErrorFormat("No ResourceExceptionRule in BU with Entity Key {0} exist.", businessUnitEntityKey);
                        }
                        else
                        {
                            totalPages=retrievalResults.TotalPages;
                            errorLevel=ErrorLevel.None;
                            Logger.DebugFormat("Retrieved ResourceExceptionRule in BU with Entity Key {0} successfully.", businessUnitEntityKey);
                            reRules.AddRange(retrievalResults.Items?.Cast<ResourceExceptionRule>().ToList());
                            pageIndex++;
                        }
                    } while (pageIndex<totalPages) ;
                return true;
                }
                catch (FaultException<TransferErrorCode> tec)
                {
                    if (tec.Detail.ErrorCode_Status=="SessionAuthenticationFailed"||tec.Detail.ErrorCode_Status=="InvalidEndpointRequest")
                    {
                        errorLevel=ErrorLevel.Transient;
                        Logger.Warn("Resetting Session Cache");
                        ResetSessionCache();
                    }
                    else
                    {
                        errorLevel=ErrorLevel.Fatal;
                        Logger.Error("TransferErrorCode caught: "+tec.Action+" | "+tec.Code.Name+" | "+tec.Detail.ErrorCode_Status+" | "+tec.Message, tec);
                        throw;
                    }
                }
                catch (EndpointNotFoundException nfex)
                {
                    errorLevel=ErrorLevel.Transient;
                    Logger.Warn("Endpoint not found. Resetting Session Cache");
                    ResetSessionCache();
                }
                catch (Exception ex)
                {
                    errorLevel=ErrorLevel.Fatal;
                    Logger.Error(ex.Message, ex);
                    throw;
                }
           
            return false;
        }

       
    }
}
