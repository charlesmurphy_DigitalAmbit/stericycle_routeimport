﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace RNAImport
{
    public class Config
    {
        internal static readonly string RNALogin = ConfigurationManager.AppSettings["LoginEmail"];
        internal static readonly string RNAPassword = ConfigurationManager.AppSettings["LoginPassword"];
        internal static readonly string RNAAppIdentifier = ConfigurationManager.AppSettings["ClientApplicationIdentifier"];
        internal static readonly int SleepDuration = Convert.ToInt32(ConfigurationManager.AppSettings["SleepDuration"]);
        internal static readonly string LogFilePath = ConfigurationManager.AppSettings["LogFilePath"];
        internal static readonly string[] RegionFilter = ConfigurationManager.AppSettings["RegionFilter"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        internal static readonly DateTime AfterDate = DateTime.Parse(ConfigurationManager.AppSettings["AfterDate"]);
        internal static readonly string BusinessUnit = ConfigurationManager.AppSettings["BusinessUnit"];
    }
}
