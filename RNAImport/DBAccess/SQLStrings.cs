using System;

namespace RNAImport.DBAccess
{
    public static class SQLStrings
    {

        private const string DB_DATE_STRING_FORMAT = "yyyy-MM-dd HH:mm:ss.fffffff";

        public static string INSERT_STAGED_ROUTE(
             string regionIdentifier,
             long regionEntityKey,
             DateTime routingSessionDate,
             string routeIdentifier,
             long routeEntityKey,
             string routeDescription,
             DateTime? startTime,
             DateTime? departureTime,
             DateTime? arrivalTime,
             DateTime? completeTime,
             double? totalPlannedDistance,
             double? totalDistance,
             TimeSpan? totalPlannedServiceTime,
             TimeSpan? totalServiceTime,
             TimeSpan? totalPlannedTravelTime,
             TimeSpan? totalTravelTime,
             TimeSpan? totalBreakTime,
             short? plannedNumberOfServiceableStops,
             short? numberOfCancelledStops,
             short? numberOfCompletedServiceableStops,
             short? numberOfMissedServiceWindows,
             short? numberOfNonServiceableStops,
             short? numberOfServiceableStops,
             short? numberOfUnknownStops,
             short? numberOfServicedOrders,
             short? numberOfServicedLineItems,
             short? numberOfServicedLineItemsScanned,
             short? numberOfStopsOutOfSequence,
             short? numberOfRouteDelays,
             TimeSpan? totalRouteDelayTime,
             short? numberOfOutOfBoundsExceptions,
             short? numberOfUnplannedStopExceptions,
             TimeSpan? totalUnplannedStopTime,
             string equipmentTypeIdentifier,
             string equipmentIdentifier,
             string workerIdentifier)
        {
            return string.Format(@"
                INSERT INTO [dbo].[{0}] (
                    [RegionIdentifier],
                    [RegionEntityKey],
                    [RoutingSessionDate],
                    [RouteIdentifier],
                    [RouteEntityKey],
                    [RouteDescription],
                    [StartTime],
                    [DepartureTime],
                    [ArrivalTime],
                    [CompleteTime],
                    [TotalPlannedDistance],
                    [TotalDistance],
                    [TotalPlannedServiceTime],
                    [TotalServiceTime],
                    [TotalPlannedTravelTime],
                    [TotalTravelTime],
                    [TotalBreakTime],
                    [PlannedNumberOfServiceableStops],
                    [NumberOfCancelledStops],
                    [NumberOfCompletedServiceableStops],
                    [NumberOfMissedServiceWindows],
                    [NumberOfNonServiceableStops],
                    [NumberOfServiceableStops],
                    [NumberOfUnknownStops],
                    [NumberOfServicedOrders],
                    [NumberOfServicedLineItems],
                    [NumberOfServicedLineItemsScanned],
                    [NumberOfStopsOutOfSequence],
                    [NumberOfRouteDelays],
                    [TotalRouteDelayTime],
                    [NumberOfOutOfBoundsExceptions],
                    [NumberOfUnplannedStopExceptions],
                    [TotalUnplannedStopTime],
                    [EquipmentTypeIdentifier],
                    [EquipmentIdentifier],
                    [WorkerIdentifier])
                VALUES (
                    '{1}',
                    {2},
                    '{3}',
                    '{4}',
                    {5},
                    '{6}',
                    {7},
                    {8},
                    {9},
                    {10},
                    {11},
                    {12},
                    {13},
                    {14},
                    {15},
                    {16},
                    {17},
                    {18},
                    {19},
                    {20},
                    {21},
                    {22},
                    {23},
                    {24},
                    {25},
                    {26},
                    {27},
                    {28},
                    {29},
                    {30},
                    {31},
                    {32},
                    {33},
                    '{34}',
                    '{35}',
                    '{36}')",
                nameof(IntegrationDBAccessor.TableName.STAGED_ROUTES),
                regionIdentifier.ToSQLParam(),
                regionEntityKey,
                routingSessionDate.ToString(Program.DATETIME_FORMAT),
                routeIdentifier.ToSQLParam(),
                routeEntityKey,
                routeDescription.ToSQLParam(),
                startTime.ToSQLParam(),
                departureTime.ToSQLParam(),
                arrivalTime.ToSQLParam(),
                completeTime.ToSQLParam(),
                totalPlannedDistance.ToSQLParam(),
                totalDistance.ToSQLParam(),
                totalPlannedServiceTime.ToSQLParam(),
                totalServiceTime.ToSQLParam(),
                totalPlannedTravelTime.ToSQLParam(),
                totalTravelTime.ToSQLParam(),
                totalBreakTime.ToSQLParam(),
                plannedNumberOfServiceableStops??0,
                numberOfCancelledStops??0,
                numberOfCompletedServiceableStops??0,
                numberOfMissedServiceWindows??0,
                numberOfNonServiceableStops??0,
                numberOfServiceableStops??0,
                numberOfUnknownStops??0,
                numberOfServicedOrders??0,
                numberOfServicedLineItems??0,
                numberOfServicedLineItemsScanned??0,
                numberOfStopsOutOfSequence??0,
                numberOfRouteDelays??0,
                totalRouteDelayTime.ToSQLParam(),
                numberOfOutOfBoundsExceptions??0,
                numberOfUnplannedStopExceptions??0,
                totalUnplannedStopTime.ToSQLParam(),
                equipmentTypeIdentifier.ToSQLParam(),
                equipmentIdentifier.ToSQLParam(),
                workerIdentifier.ToSQLParam());
        }

        private static string ToSQLParam(this DateTime dt)
        {
            return dt.ToString(Program.DATE_FORMAT);
        }

        private static string ToSQLParam(this DateTime? dt)
        {
            return dt.HasValue ? string.Format("'{0}'", dt.Value.ToSQLParam()) : "NULL";
        }

        private static string ToSQLParam(this double? d)
        {
            return d?.ToString("0.00")??"0";
        }

        private static string ToSQLParam(this string s)
        {
            return string.IsNullOrEmpty(s) ? string.Empty : s.Replace("\\\\", "\\\\\\").Replace("'", "''");
        }

        private static string ToSQLParam(this TimeSpan? ts)
        {
            return ts.HasValue ? string.Format("'{0}'", ts.Value.ToString(Program.TIMESPAN_FORMAT)) : "NULL";
        }

    }
}
