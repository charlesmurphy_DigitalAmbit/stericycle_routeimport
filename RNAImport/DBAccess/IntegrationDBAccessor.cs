using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using DBAccessUtility;
using RNAImport.DBAccess.Records;

namespace RNAImport.DBAccess
{
    public class IntegrationDBAccessor : DBAccessor
    {

        #region Private Members

        private log4net.ILog _Logger;

        #endregion

        #region Public Methods

        public enum Status
        {
            NEW,
            COMPLETE
        }

        public enum TableName
        {
            STAGED_ROUTES
        }

        public IntegrationDBAccessor(log4net.ILog logger)
            : base(ConfigurationManager.ConnectionStrings["INTEGRATION"].ConnectionString)
        {
            _Logger = logger;
        }

        public bool InsertStagedRoute(
            string regionIdentifier,
            long regionEntityKey,
            DateTime routingSessionDate,
            string routeIdentifier,
            long routeEntityKey,
            string routeDescription,
            DateTime? startTime,
            DateTime? departureTime,
            DateTime? arrivalTime,
            DateTime? completeTime,
            double? totalPlannedDistance,
            double? totalDistance,
            TimeSpan? totalPlannedServiceTime,
            TimeSpan? totalServiceTime,
            TimeSpan? totalPlannedTravelTime,
            TimeSpan? totalTravelTime,
            TimeSpan? totalBreakTime,
            short? plannedNumberOfServiceableStops,
            short? numberOfCancelledStops,
            short? numberOfCompletedServiceableStops,
            short? numberOfMissedServiceWindows,
            short? numberOfNonServiceableStops,
            short? numberOfServiceableStops,
            short? numberOfUnknownStops,
            short? numberOfServicedOrders,
            short? numberOfServicedLineItems,
            short? numberOfServicedLineItemsScanned,
            short? numberOfStopsOutOfSequence,
            short? numberOfRouteDelays,
            TimeSpan? totalRouteDelayTime,
            short? numberOfOutOfBoundsExceptions,
            short? numberOfUnplannedStopExceptions,
            TimeSpan? totalUnplannedStopTime,
            string equipmentTypeIdentifier,
            string equipmentIdentifier,
            string workerIdentifier)
        {
            bool success = true;
            try
            {
                ExecuteNonQuery(
                    SQLStrings.INSERT_STAGED_ROUTE(regionIdentifier, regionEntityKey, routingSessionDate, routeIdentifier, routeEntityKey, routeDescription, startTime, departureTime, arrivalTime, completeTime, totalPlannedDistance, totalDistance, totalPlannedServiceTime, totalServiceTime, totalPlannedTravelTime, totalTravelTime, totalBreakTime, plannedNumberOfServiceableStops, numberOfCancelledStops, numberOfCompletedServiceableStops, numberOfMissedServiceWindows, numberOfNonServiceableStops, numberOfServiceableStops, numberOfUnknownStops, numberOfServicedOrders, numberOfServicedLineItems, numberOfServicedLineItemsScanned, numberOfStopsOutOfSequence, numberOfRouteDelays, totalRouteDelayTime, numberOfOutOfBoundsExceptions, numberOfUnplannedStopExceptions, totalUnplannedStopTime, equipmentTypeIdentifier, equipmentIdentifier, workerIdentifier),
                    "Insert Staged Route ("+regionIdentifier+" | "+regionEntityKey+" | "+routingSessionDate+" | "+routeIdentifier+" | "+routeEntityKey+" | "+routeDescription+" | "+startTime+" | "+departureTime+" | "+arrivalTime+" | "+completeTime+" | "+totalPlannedDistance+" | "+totalDistance+" | "+totalPlannedServiceTime+" | "+totalServiceTime+" | "+totalPlannedTravelTime+" | "+totalTravelTime+" | "+totalBreakTime+" | "+plannedNumberOfServiceableStops+" | "+numberOfCancelledStops+" | "+numberOfCompletedServiceableStops+" | "+numberOfMissedServiceWindows+" | "+numberOfNonServiceableStops+" | "+numberOfServiceableStops+" | "+numberOfUnknownStops+" | "+numberOfServicedOrders+" | "+numberOfServicedLineItems+" | "+numberOfServicedLineItemsScanned+" | "+numberOfStopsOutOfSequence+" | "+numberOfRouteDelays+" | "+totalRouteDelayTime+" | "+numberOfOutOfBoundsExceptions+" | "+numberOfUnplannedStopExceptions+" | "+totalUnplannedStopTime+" | "+equipmentTypeIdentifier+" | "+equipmentIdentifier+" | "+workerIdentifier+")");
               
            }
            catch (DatabaseException ex)
            {
                _Logger.Error("IntegrationDBAccessor | "+ex.Message, ex);
                success=false;
            }
            return success;
        }


        #endregion

    }
}
