using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Odbc;

namespace RNAImport.DBAccess.Records
{
    public class StagedOrderRecord : DBAccessUtility.DBRecord
    {

        #region Public Properties

        public string RegionID { get; set; }
        public string OrderNumber { get; set; }

        #endregion

        #region Public Methods

        override public string ToString()
        {
            return string.Format("{0} | {1}", RegionID, OrderNumber);
        }

        override public DBAccessUtility.DBRecord Populate(OdbcDataReader reader)
        {
            return new StagedOrderRecord
            {
                RegionID = reader["RegionID"].ToString(),
                OrderNumber = reader["OrderNumber"].ToString()
            };
        }

        #endregion

    }
}
